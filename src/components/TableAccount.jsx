import React, { Component } from 'react'
import {Table,Button} from 'react-bootstrap'

export default class TableAccount extends Component {
    render() {
      return (
          <div>
              <h1>Table Account</h1>
              <Table striped bordered hover>
  <thead>
    <tr>
      <th>#</th>
      <th>Username</th>
      <th>Email</th>
      <th>Gender</th>
    </tr>
  </thead>
  <tbody>
  <tr>
      <td>1</td>
      <td>Sopheak</td>
      <td>sopheak@gmail.com</td>
      <td>female</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Hengly</td>
      <td>hengly11@gmail.com</td>
      <td>male</td>
    </tr>
    <tr>
      <td>3</td>
      <td>Yaya</td>
      <td>yaya@gmail.com</td>
      <td>female</td>
    </tr>
    <tr>
      <td>4</td>
      <td>Nita</td>
      <td>nita@gmail.com</td>
      <td>female</td>
    </tr>
  </tbody>
</Table>  
<Button variant="danger">Delete</Button>                   
          </div>
      )
   }
}
