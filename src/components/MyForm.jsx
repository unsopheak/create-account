import React, { Component } from 'react';
import { Form, Button,Image } from 'react-bootstrap';

class MyForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      username: "",
      usernameErr: "",
      genderGroup: "",
      emailErr: "",
      passwordErr: "",
      isSignin: false,
    }
  }
  onHandleText = (e) => {
    console.log(this.state);

    // let name="password"

    this.setState({
      [e.target.name]: e.target.value
    }, () => {
      // console.log(this.state);
      if(e.target.name === "genderGroup"){
        this.setState({
          genderGroup: e.target.value
        })
        // console.log(e.target.value)
      }

      if (e.target.name === "username") {
        let pattern3 = /^[\w\s-]{3,13}$/g;
        let result3 = pattern3.test(this.state.username)
        if (result3) {
          this.setState({
            usernameErr: ""
          })
        } else if (this.state.username === "") {
          this.setState({
            usernameErr: "Username cannot be empty"
          })
        } else {
          this.setState({
            usernameErr: " No more than 9 characters, No less than 3 characters"
          })
        }

      }
      if (e.target.name === "email") {
        let pattern = /^\S+@\S+\.[a-z]{3}$/g;
        let result = pattern.test(this.state.email.trim())
        if (result) {
          this.setState({
            emailErr: ""
          })
        } else if (this.state.email === "") {
          this.setState({
            emailErr: "Email cannot be empty!"
          })
        } else {
          this.setState({
            emailErr: "Email is invalid!"
          })
        }
      }

      if (e.target.name === "password") {
        let pattern2 = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_\w])([a-zA-Z0-9_\W]{8,})$/g;
        let result2 = pattern2.test(this.state.password)
        if (result2) {
          this.setState({
            passwordErr: ""
          })
        } else if (this.state.password === "") {
          this.setState({
            passwordErr: "Password cannot be empty!"
          })
        } else {
          this.setState({
            passwordErr: "Password is invalid!"
          })
        }
      }


    })

    // this.setState({
    //   email: event.target.value
    // },()=>{
    //   console.log("Email:",this.state.email);
    // })
  }

  validateBtn =()=>{
    if(this.state.usernameErr === "" && this.state.emailErr === "" && this.state.passwordErr ==="" && this.state.username !=="" && this.state.email !=="" && this.state.password !=="" ){
      return false;
    } else{
      return true;
    }

  }


  render() {
    // const {username} = this.state
    return (
      <div>
        <h1>Create Account</h1>
        <Form>
          <label htmlfor="myfile">
            <Image width="200px" height="200px" src="./img/1.png" roundedCircle alt="" /></label>
        <input id="myfile" type="file"/>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>User Name</Form.Label>
            <Form.Control
              onChange={this.onHandleText}
              name="username"
              type="username"
              placeholder="Enter Username"
            />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.usernameErr}</p>
            </Form.Text>
          </Form.Group>
          <Form.Label>Gender</Form.Label>
          {['radio'].map((type) => (
            <div key={`default-${type}`} className="mb-3">
              <Form.Check inline

                onChange={this.onHandleText}
                value="male" 
                name="genderGroup"
                type={type}
                id={`default-${type}`}
                label={`male `}
              />

              <Form.Check inline
                // disabled
                onChange={this.onHandleText}
                name="genderGroup"
                value="female" 
                type={type}
                label={`female`}
                id={`disabled-default-${type}`}
              />
            </div>
          ))}
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control name="email" onChange={this.onHandleText} type="email" placeholder="Email" />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.emailErr}</p>
            </Form.Text>
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control name="password" onChange={this.onHandleText} type="password" placeholder="Password" />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.passwordErr}</p>
            </Form.Text>
          </Form.Group>
          <br></br>
          <Button disabled={this.validateBtn()} variant="primary" type="submit">
            Save
          </Button>
        </Form>
      </div>
    );
  }
}

export default MyForm;