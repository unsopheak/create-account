import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Container,Row } from 'react-bootstrap';
import MyForm from './components/MyForm';
import Nav from "./components/Nav";
import Account from './components/Account';
import TableAccount from './components/TableAccount';

class App extends Component {
  render() {
    return (
     <Container>
       <Row>
         <Nav></Nav>
       </Row>
       {/* <Row>
         <Account></Account>
       </Row> */}
       <Row>
         <Col sm={4}>
          <MyForm/>
         </Col>
         <Col sm={8}>
          <TableAccount/>
         </Col>
       </Row>
     </Container>     
    );
  }
}

export default App;